//
//  ProjectDetail.swift
//  photo-redo-v8
//
//  Created by Fourth Dev on 11/11/2021.
//

import UIKit
import Alamofire
import SVProgressHUD

class Project {
    let id: String
    let name: String
    init(id: String, name: String) {
        self.id = id
        self.name = name
    }
    var photos: [Photo] = []
    
    func checkCachedProject(completion: @escaping (Photo)-> Void) {
        photos = []
        if UserDefaults.standard.bool(forKey: "didCache-\(id)") {
            guard let url = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask).first,
                    let listCodablePhotos = readSavedProject() else {
                        getProjectDetailFromApi(completion: completion)
                        return
                    }
            for codablePhoto in listCodablePhotos {
                let photoPath = url.appendingPathComponent("\(codablePhoto.id).jpg")
                guard let data = try? Data(contentsOf: photoPath),
                      let image = UIImage(data: data)
                else { continue }
                let photo = Photo(id: codablePhoto.id, image: image, frame: codablePhoto.frame, transform: codablePhoto.transform, alpha: codablePhoto.alpha)
                photos.append(photo)
                completion(photo)
            }
        } else {
            getProjectDetailFromApi(completion: completion)
        }
    }
    
    func getProjectDetailFromApi(completion: @escaping (Photo) -> Void) {
        SVProgressHUD.show()
        AF.request("\(Constants.endPoint)/xprojectdetail", method: .post, parameters: ["id":Int(id)]).responseJSON { response in
            guard let res = response.value as? [String: Any],
                  let resPhotos = res["photos"] as? [[String: Any]]
            else {
                SVProgressHUD.dismiss()
                return }
            var count = 0
            func checkTaskCountDone() {
                if count == resPhotos.count {
                    SVProgressHUD.dismiss()
                }
            }
            for resPhoto in resPhotos {
                guard let url = resPhoto["url"] as? String,
                      let frame = resPhoto["frame"] as? [String: Any],
                      let x = frame["x"] as? Double,
                      let y = frame["y"] as? Double,
                      let width = frame["width"] as? Double,
                      let height = frame["height"] as? Double
                else {
                    count += 1
                    checkTaskCountDone()
                    continue}
                AF.request(url).responseData { response in
                    guard let data = response.data,
                          let image = UIImage(data: data)
                    else {
                        checkTaskCountDone()
                        return
                    }
                    let photo = Photo(id: UUID().uuidString, image: image, frame: CGRect(x: x, y: y, width: width, height: height), transform: .identity, alpha: 1)
                    self.photos.append(photo)
                    completion(photo)
                    count += 1
                    checkTaskCountDone()
                }
            }
        }
    }
    
    func saveProject() {
        let listCodablePhotos: [CodablePhoto] = photos.map() {CodablePhoto(id: $0.id, image: $0.image, frame: $0.frame, transform: $0.transform, alpha: $0.alpha)}
        guard let data = try? JSONEncoder().encode(listCodablePhotos),
              let url = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask).first else { return }
        let fileUrl = url.appendingPathComponent("listPhoto\(self.id).json")
        try? data.write(to: fileUrl)
        for photo in photos {
            if let data = photo.image.jpegData(compressionQuality: 1) {
                let photoPath = url.appendingPathComponent("\(photo.id).jpg")
                try? data.write(to: photoPath)
            }
        }
        UserDefaults.standard.set(true, forKey: "didCache-\(id)")
        ListProjects.shared.saveListProjects()
        
    }
    
    func readSavedProject() -> [CodablePhoto]? {
        guard let url = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask).first else {return nil}
        let path = url.appendingPathComponent("listPhoto\(self.id).json")
        guard let data = try? Data(contentsOf: path),
              let listCodablePhotos = try? JSONDecoder().decode([CodablePhoto].self, from: data) else { return nil}
        return listCodablePhotos
        
    }
}
