//
//  Entity.swift
//  photo-redo-v8
//
//  Created by Fourth Dev on 11/11/2021.
//

import UIKit

class CodableProject: Codable {
    let id: String
    let name: String
    init(id: String, name: String) {
        self.id = id
        self.name = name
    }
}

class Photo {
    let id: String
    let image: UIImage
    var frame: CGRect
    var transform: CGAffineTransform
    var alpha: CGFloat
    
    init(id: String, image: UIImage, frame: CGRect, transform: CGAffineTransform, alpha: CGFloat) {
        self.id = id
        self.image = image
        self.frame = frame
        self.transform = transform
        self.alpha = alpha
    }
    
    func save(_ image: CustomImageView) {
        transform = image.transform
        alpha = image.alpha
    }
}

class CodablePhoto: Codable {
    let id: String
    let frame: CGRect
    let transform: CGAffineTransform
    let alpha: CGFloat
    
    init(id: String, image: UIImage, frame: CGRect, transform: CGAffineTransform, alpha: CGFloat) {
        self.id = id
        self.frame = frame
        self.transform = transform
        self.alpha = alpha
    }
}
