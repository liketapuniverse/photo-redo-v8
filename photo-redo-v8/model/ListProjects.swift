//
//  ListProjects.swift
//  photo-redo-v8
//
//  Created by Fourth Dev on 11/11/2021.
//

import UIKit
import Alamofire
import SVProgressHUD

class ListProjects {
    static var shared = ListProjects()
    var projects: [Project] = []
    init() {
        checkSavedProjects()
    }
    
    func checkSavedProjects() {
        if let listCodableProjects = readSavedListProjects() {
            projects = listCodableProjects.map(){ Project(id: $0.id, name: $0.name) }
            NotificationCenter.default.post(name: NSNotification.Name(Constants.didLoadProjects), object: nil)
        } else {
            getProjectsFromApi()
        }
    }
    
    func getProjectsFromApi() {
        SVProgressHUD.show()
        AF.request("\(Constants.endPoint)/xproject").responseJSON { response in
            guard let res = response.value as? [String: Any],
                  let resProjects = res["projects"] as? [[String: Any]]
            else {
                SVProgressHUD.dismiss()
                return }
            for resProject in resProjects {
                guard let id = resProject["id"] as? Int,
                      let name = resProject["name"] as? String
                else { continue }
                self.projects.append(Project(id: String(id), name: name))
                
            }
            NotificationCenter.default.post(name: NSNotification.Name(Constants.didLoadProjects), object: nil)
            SVProgressHUD.dismiss()
        }
    }
    
    func saveListProjects() {
        let listCodableProjects = projects.map(){CodableProject(id: $0.id, name: $0.name)}
        guard let url = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask).first,
            let encodedString = try? JSONEncoder().encode(listCodableProjects) else { return }
        let path = url.appendingPathComponent(Constants.listProjects)
         try? encodedString.write(to: path)
    }
    
    func readSavedListProjects() -> [CodableProject]? {
        guard let url = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask).first else { return nil }
        let path = url.appendingPathComponent(Constants.listProjects)
        guard let data = try? Data(contentsOf: path),
              let listCodableProjects = try? JSONDecoder().decode([CodableProject].self, from: data)
        else { return nil}
        return listCodableProjects
    }
}
