//
//  Constants.swift
//  photo-redo-v8
//
//  Created by Fourth Dev on 11/11/2021.
//

import Foundation

class Constants {
    static let listProjects = "listProjects.json"
    static let didLoadProjects = "didLoadProjects"
    static let endPoint = "https://tapuniverse.com"
}
