//
//  ViewController.swift
//  photo-redo-v8
//
//  Created by Fourth Dev on 11/11/2021.
//

import UIKit

class Screen1ViewController: UIViewController {
    var model = ListProjects.shared
    @IBOutlet weak var projectTableView: UITableView!
    override func viewDidLoad() {
        super.viewDidLoad()
        NotificationCenter.default.addObserver(forName: NSNotification.Name(Constants.didLoadProjects), object: nil, queue: nil) { [weak self] _ in
            DispatchQueue.main.async {
                self?.projectTableView.reloadData()
            }
        }
    }
    
    deinit {
        NotificationCenter.default.removeObserver(self)
    }

    @IBAction func addProject(_ sender: Any) {
        DispatchQueue.main.async {
            let alert = UIAlertController(title: "Add Project", message: "Enter a name", preferredStyle: .alert)
            alert.addTextField { textField in
                textField.placeholder = "Enter project name"
            }
            let cancelAction = UIAlertAction(title: "Cancel", style: .cancel) { _ in
                
            }
            let OkAction = UIAlertAction(title: "Ok", style: .default) { _ in
                guard let textField = alert.textFields?.first else { return }
                if textField.text?.trimmingCharacters(in: .whitespacesAndNewlines) == "" {
                    let emptyAlert = UIAlertController(title: "Empty name", message: "Please don't leave name blank", preferredStyle: .alert)
                    emptyAlert.addAction(UIAlertAction(title: "Ok", style: .default, handler: nil))
                    self.present(emptyAlert, animated: true, completion: nil)
                } else {
                    guard let text = textField.text else { return}
                    let project = Project(id: UUID().uuidString, name: text)
                    self.model.projects.append(project)
                    self.projectTableView.reloadData()
                    self.model.saveListProjects()
                }
            }
            alert.addAction(cancelAction)
            alert.addAction(OkAction)
            self.present(alert, animated: true, completion: nil)
        }
    }
}

extension Screen1ViewController: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return model.projects.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: "ProjectTableViewCell") as? ProjectTableViewCell else { return UITableViewCell() }
        cell.projectNameLabel.text = model.projects[indexPath.row].name
        cell.delegate = self
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        guard let vc = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "Screen2ViewController") as? Screen2ViewController else { return }
        vc.project = model.projects[indexPath.row]
        self.navigationController?.pushViewController(vc, animated: true)
    }
}

extension Screen1ViewController: ProjectTableViewCellDelegate {
    func deleteCell(_ cell: ProjectTableViewCell) {
        if let indexPath = projectTableView.indexPath(for: cell) {
            model.projects.remove(at: indexPath.row)
            model.saveListProjects()
            projectTableView.reloadData()
        }
    }
    
    
}
