//
//  ProjectTableViewCell.swift
//  photo-redo-v8
//
//  Created by Fourth Dev on 11/11/2021.
//

import UIKit
protocol ProjectTableViewCellDelegate: AnyObject {
    func deleteCell(_ cell: ProjectTableViewCell)
}

class ProjectTableViewCell: UITableViewCell {

    @IBOutlet weak var projectNameLabel: UILabel!
    @IBOutlet weak var deleteButton: UIButton!
    @IBOutlet weak var trailingConstant: NSLayoutConstraint!
    var beginX: CGFloat = 0
    var didLoad = false
    
    weak var delegate: ProjectTableViewCellDelegate?
    override func draw(_ rect: CGRect) {
        if didLoad { return }
        if #available(iOS 13, *) {} else {
            deleteButton.setImage(UIImage(named: "minus30"), for: .normal)
        }
        trailingConstant.constant = 0
        let pan = UIPanGestureRecognizer(target: self, action: #selector(didPan(_:)))
        addGestureRecognizer(pan)
        pan.delegate = self
        didLoad = true
    }
    
    override func prepareForReuse() {
        trailingConstant.constant = 0
    }
    
    override func gestureRecognizerShouldBegin(_ gestureRecognizer: UIGestureRecognizer) -> Bool {
        guard let g = gestureRecognizer as? UIPanGestureRecognizer else { return true}
        let point = g.velocity(in: self)
        return abs(point.x)  > abs(point.y)
    }
    
    @objc func didPan(_ g: UIPanGestureRecognizer) {
        if g.state == .began {
            beginX = trailingConstant.constant
        }
        if g.state == .changed {
            let translation = g.translation(in: self)
            trailingConstant.constant = beginX - translation.x
        }
        if g.state == .ended || g.state == .cancelled {
            var currentTranslation = trailingConstant.constant
            currentTranslation = max(0, currentTranslation)
            currentTranslation = min(50, currentTranslation)
            currentTranslation = currentTranslation < 20 ? 0 : 50
            trailingConstant.constant = currentTranslation
            UIView.animate(withDuration: 0.2) {
                self.layoutIfNeeded()
            }

        }
    }
    
    
    @IBAction func deleteProject(_ sender: Any) {
        if let d = delegate {
            d.deleteCell(self)
        }
    }
}
