//
//  PhotoCollectionViewCell.swift
//  photo-redo-v8
//
//  Created by Fourth Dev on 11/11/2021.
//

import UIKit

class PhotoCollectionViewCell: UICollectionViewCell {
    override var isSelected: Bool {
        didSet {
            layer.borderWidth = isSelected ? 2 : 0
            layer.borderColor = (UIColor(named: "appBlue") ?? UIColor.blue).cgColor
        }
    }
    @IBOutlet weak var imageView: UIImageView!
}
