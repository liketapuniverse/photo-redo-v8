//
//  Screen3ViewController.swift
//  photo-redo-v8
//
//  Created by Fourth Dev on 11/11/2021.
//

import UIKit
import Photos
protocol CustomPhotoPickerDelegate: AnyObject {
    func imagePicked(_ image: UIImage)
}

class Screen3ViewController: UIViewController {
    let fetchOption = PHFetchOptions()
    let numberCellPerRow = 3
    @IBOutlet weak var photoCollectionView: UICollectionView!
    @IBOutlet weak var albumMenuButton: UIButton!
    @IBOutlet weak var addButton: UIButton!
    var listAlbum: [Album] = []
    var listCollections = PHFetchResult<PHAssetCollection>()
    var currentAssetAlbum = PHFetchResult<PHAsset>()
    weak var delegate: CustomPhotoPickerDelegate?
    override func viewDidLoad() {
        super.viewDidLoad()
        photoCollectionView.allowsMultipleSelection = true
        addButton.isEnabled = false
        addButton.setTitleColor(UIColor(named: "appBlue") ?? UIColor.blue, for: .normal)
        addButton.setTitleColor(UIColor.gray, for: .disabled)
        fetchOption.sortDescriptors = [NSSortDescriptor(key: "creationDate", ascending: false)]
        fetchAlbum {
            self.photoCollectionView.reloadData()
            self.setAlbumTitles()
        }
    }
    
    func fetchAlbum(completion: @escaping ()-> Void) {
        let status = PHPhotoLibrary.authorizationStatus()
        if status == .notDetermined {
            PHPhotoLibrary.requestAuthorization { status in
                if status == .authorized {
                    self.fetchPhotosOnAuthorized(completion: completion)
                } else if status == .denied {
                    self.onPhotoPermissionDenied()
                }
            }
        } else if status == .authorized {
            fetchPhotosOnAuthorized(completion: completion)
        } else if status == .denied {
            onPhotoPermissionDenied()
        }
    }
    
    func onPhotoPermissionDenied() {
        DispatchQueue.main.async {
            let alert = UIAlertController(title: "Please allow access to photos", message: "Permission needed", preferredStyle: .alert)
            alert.addAction(UIAlertAction(title: "Cancel", style: .default, handler: nil))
            alert.addAction(UIAlertAction(title: "Go to Settings", style: .cancel, handler: { _ in
                guard let url = URL(string: UIApplication.openSettingsURLString),
                      UIApplication.shared.canOpenURL(url) else { return }
                UIApplication.shared.open(url, options: [:], completionHandler: nil)
            }))
            self.present(alert, animated: true, completion: nil)
        }
    }
    
    func setAlbumTitles() {
        if #available(iOS 14.0, *) {
            let listElement: [UIMenuElement] = listAlbum.map { album in
                UIAction(title: album.name) { _ in
                    self.albumDidChange(album) { name in
                        self.photoCollectionView.reloadData()
                        self.albumMenuButton.setTitle(name, for: .normal)
                    }
                }
            }
            albumMenuButton.menu = UIMenu(title: "", options: .displayInline, children: listElement)
            albumMenuButton.showsMenuAsPrimaryAction = true
        }
    }
    
    func albumDidChange(_ album: Album, completion: (String)-> Void) {
        if album.index == 0 {
            currentAssetAlbum = PHAsset.fetchAssets(with: .image, options: fetchOption)
        } else {
            currentAssetAlbum = PHAsset.fetchAssets(in: listCollections[album.index - 1], options: fetchOption)
        }
        completion(album.name)
    }
    
    func fetchPhotosOnAuthorized(completion: @escaping ()-> Void) {
        currentAssetAlbum = PHAsset.fetchAssets(with: .image, options: fetchOption)
        listAlbum.append(Album(index: 0, name: "Recent Photos"))
        listCollections = PHAssetCollection.fetchAssetCollections(with: .album, subtype: .albumRegular, options: nil)
        for i in 0..<listCollections.count {
            if let name = listCollections[i].localizedTitle {
                listAlbum.append(Album(index: (i + 1), name: name))
            }
        }
        completion()
    }
    
    func getImageFromAsset(asset: PHAsset, isFullSize: Bool? = false) -> UIImage? {
        let manager = PHImageManager.default()
        let option = PHImageRequestOptions()
        var thumbnail: UIImage?
        option.isSynchronous = true
        let targetSize = (isFullSize ?? false) ? PHImageManagerMaximumSize : CGSize(width: 300, height: 300)
        manager.requestImage(for: asset, targetSize: targetSize, contentMode: .aspectFit, options: option) { result, info in
            thumbnail = result
        }
        return thumbnail
    }
    
    @IBAction func onCancel(_ sender: Any) {
        dismiss(animated: true, completion: nil)
    }
    
    @IBAction func addPhoto(_ sender: Any) {
        if let listIndexPath = photoCollectionView.indexPathsForSelectedItems {
            for indexPath in listIndexPath {
                guard let image = getImageFromAsset(asset: currentAssetAlbum[indexPath.row], isFullSize: true) else { continue}
                if let d = delegate {
                    d.imagePicked(image)
                }
            }
        }
        dismiss(animated: true)
    }
}

extension Screen3ViewController: UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return currentAssetAlbum.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        guard let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "PhotoCollectionViewCell", for: indexPath) as? PhotoCollectionViewCell else { return PhotoCollectionViewCell()}
        cell.imageView.image = getImageFromAsset(asset: currentAssetAlbum[indexPath.row])
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        guard let layout = collectionViewLayout as? UICollectionViewFlowLayout else { return .zero}
        let totalSpace = layout.sectionInset.left + layout.sectionInset.right + layout.minimumInteritemSpacing*CGFloat(numberCellPerRow - 1)
        let size = (collectionView.bounds.width - totalSpace)/CGFloat(numberCellPerRow)
        return CGSize(width: size, height: size)
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        checkAddButtonEnable()
    }
    
    func collectionView(_ collectionView: UICollectionView, didDeselectItemAt indexPath: IndexPath) {
        checkAddButtonEnable()
    }
    
    func checkAddButtonEnable() {
        if let listIndexPath = photoCollectionView.indexPathsForSelectedItems {
            addButton.isEnabled = listIndexPath.count > 0
        }
    }
}

class Album {
    let index: Int
    let name: String
    
    init(index: Int, name: String) {
        self.index = index
        self.name = name
    }
}
