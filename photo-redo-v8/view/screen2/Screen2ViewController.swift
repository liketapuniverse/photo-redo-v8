//
//  Screen2ViewController.swift
//  photo-redo-v8
//
//  Created by Fourth Dev on 11/11/2021.
//

import UIKit
import SVProgressHUD
import Photos

class Screen2ViewController: UIViewController {
    var project: Project!
    @IBOutlet weak var exportButton: UIButton!
    @IBOutlet weak var scrollView: UIScrollView!
    @IBOutlet weak var galleryView: GalleryView!
    @IBOutlet weak var sliderView: SliderView!
    var tempImage: UIImage? = nil
    override func viewDidLoad() {
        super.viewDidLoad()
        galleryView.project = project
        galleryView.delegate = self
        sliderView.isHidden = true
        sliderView.delegate = self
        galleryView.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(didTapOutsideGallery)))
        project.checkCachedProject { photo in
            self.galleryView.presentPhoto(photo)
        }
    }
    
    @objc func didTapOutsideGallery() {
        galleryView.selected = nil
        galleryView.currentImage = nil
        galleryView.setBorderHidden(true)
        galleryView.deleteButton.isHidden = true
        sliderView.isHidden = true
    }
    
    @IBAction func onBack(_ sender: Any) {
        SVProgressHUD.dismiss()
        DispatchQueue.main.async {
            let alert = UIAlertController(title: "Save Project", message: "Save changes to this project?", preferredStyle: .alert)
            let cancelAction = UIAlertAction(title: "Cancel", style: .cancel) { _ in
                self.navigationController?.popViewController(animated: true)
            }
            let okAction = UIAlertAction(title: "OK", style: .default) { _ in
                self.project.saveProject()
                self.navigationController?.popViewController(animated: true)
            }
            alert.addAction(cancelAction)
            alert.addAction(okAction)
            self.present(alert, animated: true, completion: nil)
        }
    }
    
    func onPhotoPermissionDenied() {
        DispatchQueue.main.async {
            let alert = UIAlertController(title: "Please allow access to photos", message: "Permission needed", preferredStyle: .alert)
            alert.addAction(UIAlertAction(title: "Cancel", style: .default, handler: nil))
            alert.addAction(UIAlertAction(title: "Go to Settings", style: .cancel, handler: { _ in
                guard let url = URL(string: UIApplication.openSettingsURLString),
                      UIApplication.shared.canOpenURL(url) else { return }
                UIApplication.shared.open(url, options: [:], completionHandler: nil)
            }))
            self.present(alert, animated: true, completion: nil)
        }
    }
    
    @IBAction func exportPhoto(_ sender: Any) {
        let status = PHPhotoLibrary.authorizationStatus()
        if status == .notDetermined {
            PHPhotoLibrary.requestAuthorization { status in
                if status == .denied {
                    self.onPhotoPermissionDenied()
                }
                if status == .authorized {
                    self.galleryView.exportPhoto()
                }
            }
        } else if status == .denied { onPhotoPermissionDenied() }
        else {
            galleryView.exportPhoto()
        }
        
    }
    @IBAction func addPhoto(_ sender: Any) {
        guard let vc = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "Screen3ViewController") as? Screen3ViewController else { return }
        vc.delegate = self
        self.navigationController?.present(vc, animated: true, completion: nil)
    }
}

extension Screen2ViewController: CustomPhotoPickerDelegate {
    func imagePicked(_ image: UIImage) {
        galleryView.addPhoto(image)
    }
}

extension Screen2ViewController: UIScrollViewDelegate {
    func viewForZooming(in scrollView: UIScrollView) -> UIView? {
        return galleryView
    }
    func scrollViewDidZoom(_ scrollView: UIScrollView) {
        reloadView(scrollView)
    }
    func scrollViewDidEndZooming(_ scrollView: UIScrollView, with view: UIView?, atScale scale: CGFloat) {
        reloadView(scrollView)
    }
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        reloadView(scrollView)
    }
    func reloadView(_ scrollView: UIScrollView) {
        galleryView.currentZoomScale = scrollView.zoomScale
        galleryView.reBuildViewInScrollView()
    }
}

extension Screen2ViewController: GalleryViewDelegate {
    func imageSelected(_ image: CustomImageView) {
        sliderView.isHidden = false
        sliderView.value = image.alpha
        sliderView.setNeedsDisplay()
    }
    
    func imageExport(_ image: UIImage) {
        tempImage = image
        UIImageWriteToSavedPhotosAlbum(image, self, #selector(self.writeImageResult(_:didFinishSavingWithError:contextInfo:)), nil)
    }
    
    @objc func writeImageResult(_ image: UIImage, didFinishSavingWithError error: Error?, contextInfo: UnsafeRawPointer) {
        if let error = error {
            DispatchQueue.main.async {
                let errorAlert = UIAlertController(title: "Error", message: "Error occur", preferredStyle: .alert)
                errorAlert.addAction(UIAlertAction(title: "Ok", style: .default, handler: nil))
                self.present(errorAlert, animated: true, completion: nil)
            }
        } else {
            DispatchQueue.main.async {
                let ac = UIAlertController(title: "Saved!", message: "Photo was saved successfully", preferredStyle: .alert)
                let shareAction = UIAlertAction(title: "Share now", style: .default) { _ in
                    let activityViewController = UIActivityViewController(activityItems: [self.tempImage], applicationActivities: nil)
                    self.present(activityViewController, animated: true, completion: nil)
                }
                ac.addAction(UIAlertAction(title: "OK", style: .default))
                ac.addAction(shareAction)
                self.present(ac, animated: true)
            }
        }
    }
}

extension Screen2ViewController: SliderViewDelegate {
    func alphaChanged(_ alpha: Double) {
        galleryView.updateOpacity(alpha)
    }
}
