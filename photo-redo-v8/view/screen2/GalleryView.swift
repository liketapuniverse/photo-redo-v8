//
//  GalleryView.swift
//  photo-redo-v8
//
//  Created by Fourth Dev on 11/11/2021.
//

import UIKit
protocol GalleryViewDelegate: AnyObject {
    func imageExport(_ image: UIImage)
    func imageSelected(_ image: CustomImageView)
}
class GalleryView: UIView {
    var project: Project!
    var currentZoomScale: Double = 1
    var selected: Int? = -1
    var currentImage: CustomImageView? = nil
    var listId: [String] = []
    let deleteButton = CustomButton()
    let deleteButtonWidth: CGFloat = 20
    let line = CAShapeLayer()
    let circle = CAShapeLayer()
    var didLoad = false
    weak var delegate: GalleryViewDelegate?
    override func draw(_ rect: CGRect) {
        if didLoad { return}
        setUpDeleteButton()
        didLoad = true
    }
    
    func setUpDeleteButton() {
        deleteButton.isHidden = true
        deleteButton.frame = CGRect(x: 0, y: 0, width: deleteButtonWidth, height: deleteButtonWidth)
        deleteButton.layer.cornerRadius = deleteButtonWidth/2
        deleteButton.backgroundColor = .red
        let centerWhiteView = UIView()
        centerWhiteView.frame = CGRect(x: 0, y: 0, width: 8, height: 2)
        centerWhiteView.backgroundColor = .white
        deleteButton.addSubview(centerWhiteView)
        centerWhiteView.center = CGPoint(x: deleteButton.bounds.midX, y: deleteButton.bounds.midY)
        deleteButton.addTarget(self, action: #selector(deletePhoto), for: .touchUpInside)
        addSubview(deleteButton)
    }
    
    func presentPhoto(_ photo: Photo) {
        let image = CustomImageView(image: photo.image)
        image.id = photo.id
        image.frame = photo.frame
        image.transform = photo.transform
        image.alpha = photo.alpha
        addSubview(image)
        setUpImageView(image)
        listId.append(photo.id)
    }
    
    func addPhoto(_ image: UIImage) {
        let pickedImage = CustomImageView(image: image)
        pickedImage.id = UUID().uuidString
        listId.append(pickedImage.id)
        var width = bounds.width/2
        let childHeightWidthRatio = pickedImage.bounds.height/pickedImage.bounds.width
        var height = width*childHeightWidthRatio
        let parentHeightWidthRatio = bounds.height/self.bounds.width
        let downSizeRatio = childHeightWidthRatio/parentHeightWidthRatio
        if downSizeRatio > 1 {
            width /= downSizeRatio
            height /= downSizeRatio
        }
        pickedImage.frame = CGRect(x: bounds.midX - width/2, y: bounds.midY - height/2, width: width, height: height)
        addSubview(pickedImage)
        setUpImageView(pickedImage)
        
        let photo = Photo(id: pickedImage.id, image: image, frame: pickedImage.frame, transform: .identity, alpha: 1)
        project.photos.append(photo)
        
    }
    
    @objc func deletePhoto() {
        guard let i = selected else { return }
        deleteButton.isHidden = true
        setBorderHidden(true)
        project.photos.remove(at: i)
        currentImage?.removeFromSuperview()
        
    }
    
    func exportPhoto() {
        if let d = delegate {
            let image = drawImageWithSize(CGSize(width: bounds.width, height: bounds.height))
            d.imageExport(image)
        }
    }
    
    func drawImageWithSize(_ size: CGSize) -> UIImage {
        let format = UIGraphicsImageRendererFormat()
        let renderer = UIGraphicsImageRenderer(size: size, format: format)
        let image = renderer.image { ctx in
            ctx.cgContext.setFillColor((UIColor(named: "galleryBackground") ?? UIColor.white).cgColor)
            ctx.fill(CGRect(origin: .zero, size: CGSize(width: bounds.width, height: bounds.height)))
            var newPhotos: [Photo] = []
            for child in self.subviews {
                if let photo = child as? CustomImageView {
                    project.photos.forEach { oldPhoto in
                        if oldPhoto.image == photo.image {
                            newPhotos.append(oldPhoto)
                        }
                    }
                }
            }
            for photo in newPhotos {
                let t = photo.transform.concatenating(CGAffineTransform(translationX: photo.frame.midX, y: photo.frame.midY))
                let rect = CGRect(x: -photo.frame.width/2, y: -photo.frame.height/2, width: photo.frame.width, height: photo.frame.height)
                ctx.cgContext.concatenate(t)
                photo.image.draw(in: rect, blendMode: .normal, alpha: photo.alpha)
                ctx.cgContext.concatenate(t.inverted())
            }
        }
        return  image
    }
    
    func updateOpacity(_ alpha: Double) {
        guard let image = currentImage,
        let selected = selected
        else { return }
        image.alpha = alpha
        project.photos[selected].save(image)
    }
}

//MARK: Photo Gesture
extension GalleryView {
    func notifyChange(_ image: CustomImageView) {
        guard let index = listId.firstIndex(of: image.id) else { return }
        selected = index
        currentImage = image
        bringSubviewToFront(image)
        drawBorder(image)
        updateDeleteButtonFrame(image)
        bringSubviewToFront(deleteButton)
        project.photos[index].save(image)
        if let d = delegate {
            d.imageSelected(image)
        }
        
    }
    
    func updateDeleteButtonFrame(_ image: CustomImageView) {
        deleteButton.isHidden = false
        let imageTransform = image.transform
        let scale = sqrt(pow(imageTransform.a, 2) + pow(imageTransform.c, 2))*currentZoomScale
        let center = image.convert(CGPoint(x: image.bounds.midX, y: -15/scale), to: self)
        deleteButton.center = center
        deleteButton.transform = self.transform.inverted()
    }
    
    func setUpImageView(_ image: CustomImageView) {
        image.isUserInteractionEnabled = true
        let tap = UITapGestureRecognizer(target: self, action: #selector(didTap(_:)))
        tap.delegate = image
        image.addGestureRecognizer(tap)
        
        let pan = UIPanGestureRecognizer(target: self, action: #selector(didPan(_:)))
        pan.delegate = image
        image.addGestureRecognizer(pan)
        
        let pinch = UIPinchGestureRecognizer(target: self, action: #selector(didPinch(_:)))
        pinch.delegate = image
        image.addGestureRecognizer(pinch)
        
        let rotate = UIRotationGestureRecognizer(target: self, action: #selector(didRotate(_:)))
        rotate.delegate = image
        image.addGestureRecognizer(rotate)
        
    }
    
    func reBuildViewInScrollView() {
        if let image = currentImage {
            updateDeleteButtonFrame(image)
            drawBorder(image)
        }
    }
    
    @objc func didTap(_ g: UITapGestureRecognizer) {
        guard let image = g.view as? CustomImageView else { return}
        notifyChange(image)
    }
    
    @objc func didPan(_ g: UIPanGestureRecognizer) {
        guard let image = g.view as? CustomImageView else { return}
//        print("pan called")
        if g.state == .began {
            image.beginT = image.transform
        }
        if g.state == .changed {
            let translation = g.translation(in: image)
            image.transform = image.beginT.translatedBy(x: translation.x, y: translation.y)
        }
        notifyChange(image)
    }
    
    @objc func didPinch(_ g: UIPinchGestureRecognizer) {
        guard let image = g.view as? CustomImageView else { return}
        if g.state == .began {
            image.beginT = image.transform
        }
        if g.state == .changed {
            image.scale = g.scale
            image.transform = image.beginT
                .rotated(by: image.rotation)
                .scaledBy(x: image.scale, y: image.scale)
        }
        if g.state == .ended || g.state == .cancelled {
            image.scale = 1
        }
        notifyChange(image)
    }
    
    @objc func didRotate(_ g: UIRotationGestureRecognizer) {
        guard let image = g.view as? CustomImageView else { return}
        if g.state == .began {
            image.beginT = image.transform
        }
        if g.state == .changed {
            image.rotation = g.rotation
            image.transform = image.beginT.rotated(by: image.rotation)
                .scaledBy(x: image.scale, y: image.scale)
        }
        if g.state == .ended || g.state == .cancelled {
            image.rotation = 0
        }
        notifyChange(image)
    }
}

class CustomImageView: UIImageView, UIGestureRecognizerDelegate {
    var beginT: CGAffineTransform = .identity
    var rotation: CGFloat = 0
    var scale: CGFloat = 1
    var id: String = ""
    
    func gestureRecognizer(_ gestureRecognizer: UIGestureRecognizer, shouldRecognizeSimultaneouslyWith otherGestureRecognizer: UIGestureRecognizer) -> Bool {
        let hasPinch = gestureRecognizer is UIPinchGestureRecognizer ||
                        otherGestureRecognizer is UIPinchGestureRecognizer
        let hasRotation = gestureRecognizer is UIRotationGestureRecognizer ||
                        otherGestureRecognizer is UIRotationGestureRecognizer
        
        return hasPinch && hasRotation
    }
}

//MARK: Border LIne

extension GalleryView {
    func setBorderHidden(_ hidden: Bool) {
        line.isHidden = hidden
        circle.isHidden = hidden
    }
    
    func drawBorder(_ image: CustomImageView) {
        let topLeft = image.convert(CGPoint(x: 0, y: 0), to: self)
        let topRight = image.convert(CGPoint(x: image.bounds.maxX, y: 0), to: self)
        let bottomRight = image.convert(CGPoint(x: image.bounds.maxX, y: image.bounds.maxY), to: self)
        let bottomLeft = image.convert(CGPoint(x: 0, y: image.bounds.maxY), to: self)
        let linePath = UIBezierPath()
        linePath.move(to: topLeft)
        linePath.addLine(to: topRight)
        linePath.addLine(to: bottomRight)
        linePath.addLine(to: bottomLeft)
        linePath.addLine(to: topLeft)
        linePath.close()
        line.path = linePath.cgPath
        
        line.strokeColor = (UIColor(named: "appBlue") ?? UIColor.blue).cgColor
        line.lineWidth = 4/currentZoomScale
        line.fillColor = nil
        
        let circlePath = UIBezierPath()
        for p in [topLeft, topRight, bottomRight, bottomLeft] {
            circlePath.append(UIBezierPath(arcCenter: p, radius: 10/currentZoomScale, startAngle: 0, endAngle: .pi*2, clockwise: true))
        }
        circle.path = circlePath.cgPath
        circle.fillColor = (UIColor(named: "appBlue") ?? UIColor.blue).cgColor
        setBorderHidden(false)
        layer.addSublayer(line)
        layer.addSublayer(circle)
    }
}

class CustomButton: UIButton {
    override func point(inside point: CGPoint, with event: UIEvent?) -> Bool {
        return bounds.insetBy(dx: -10, dy: -10).contains(point)
    }
}

